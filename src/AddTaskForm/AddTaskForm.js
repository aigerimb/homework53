import React from "react";
import "./AddTaskForm.css";

const AddTaskForm = props => {
  return (
    <div className="add_task_form">
      <input type="text" name="newTask" className="addTaskInput" onChange={props.inputHandler}/>
      <button className="add_btn" onClick={props.addTask}>Add</button>
    </div>
  );
};

export default AddTaskForm;