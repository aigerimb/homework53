import './App.css';
import {Component} from "react";
import AddTaskForm from "./AddTaskForm/AddTaskForm";
import {v1 as uuid} from "uuid";
import Task from "./Task/Task";
import DoneTask from "./DoneTask/DoneTask";

class App extends Component {
  state = {
    tasks: [
      {text: "Walk with dog", id: uuid(), checked: false},
      {text: "Do homework", id: uuid(), checked: false}
    ],
    done: [
      {text: "Buy milk", id: uuid(), checked: true}
    ],
    newTask: ""
  }

  inputHandler = (e) => {
    this.setState({[e.target.name]: e.target.value});
  };

  addTask = () => {
    const newTask = {};
    newTask.text = this.state.newTask;
    newTask.id = uuid();
    const tasks = [...this.state.tasks, newTask];
    this.setState({tasks});
  };

  deleteTask = (id) => {
    const tasks = [...this.state.tasks];
    const index = tasks.findIndex(task => task.id === id);
    tasks.splice(index, 1);

    this.setState({tasks});
  };

  deleteDoneTask = (id) => {
    const done = [...this.state.done];
    const index = done.findIndex(task => task.id === id);
    done.splice(index, 1);

    this.setState({done});
  };

  setChecked = (id) => {
    const tasks = [...this.state.tasks];
    const index = tasks.findIndex(task => task.id === id);
    const done = [tasks[index], ...this.state.done];
    tasks.splice(index,  1);
    this.setState({tasks, done});
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Task manager</h1>
        </header>
        <AddTaskForm
          inputHandler={this.inputHandler}
          addTask={this.addTask}
        />
        <div className="tasks_wrapper">
        {
          <div className="tasks">
            <h2 className="title">To do</h2>
            {
              this.state.tasks.map((task) => {
                return <Task
                  key={task.id}
                  text={task.text}
                  deleteTask={() => this.deleteTask(task.id)}
                  setChecked={() => this.setChecked(task.id)}
                />
              })
            }
          </div>
        }
        {
          <div className="tasks">
            <h2 className="title">Already done</h2>
            {
              this.state.done.map((task) => {
                return <DoneTask
                  key={task.id}
                  text={task.text}
                  deleteTask={() => this.deleteDoneTask(task.id)}
                />
              })
            }
          </div>
        }
        </div>
      </div>
    );
  }
}

export default App;
