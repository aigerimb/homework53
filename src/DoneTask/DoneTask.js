import React from "react";
import icon from "../delete.png";

const DoneTask = (props) => {
  return (
    <div className="task">
      <p className="task_text">{props.text}</p>
      <button className="delete_btn" onClick={props.deleteTask}>
        <img src={icon} />
      </button>
    </div>
  );
};

export default DoneTask;