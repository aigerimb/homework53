import React from "react";
import "./Task.css";
import icon from "../delete.png";

const Task = props => {
  return (
    <div className="task">
      <input type="checkbox" id="taskText" className="input_field" onChange={props.setChecked} />
      <label className="task_text" htmlFor="taskText">{props.text}</label>
      <button className="delete_btn" onClick={props.deleteTask}>
        <img src={icon} />
      </button>
    </div>
  );
};

export default Task;